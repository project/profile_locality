Drupal.behaviors.profile_locality = function (context) {
  var curCountry = $(countryId).val();
  // codes array contains iso country codes w/ postal codes available
  if (jQuery.inArray(curCountry, codes) == -1) {
    // hide postal input element when current country selection does not have postal codes available
    $(postalId + '-wrapper').hide();
  }
  
  $(countryId).change( function() {
    var newCountry = $(this).val();
    if (jQuery.inArray(newCountry, codes) == -1) {
      $(postalId + '-wrapper').slideUp();
      curCountry = newCountry;
    }
    else if (newCountry != curCountry) {
      $(postalId + '-wrapper').slideDown();
      // unbind previous event on postal input element and empty its value
      $(postalId).unbind().val('');
      // define object for hidden postal autocomplete input element
      var postal_autocomplete = $(postalId + '-autocomplete');
      // replace previous iso code w/ new country selection
      postal_autocomplete.val(postal_autocomplete.val().substr(0, postal_autocomplete.val().lastIndexOf('/') + 1) + newCountry);
    
      curCountry = newCountry;
      postal_autocomplete.removeClass('autocomplete-processed');
      Drupal.behaviors.autocomplete(document);
    }
    
    return false;
  }).addClass('autocomplete-processed');
}